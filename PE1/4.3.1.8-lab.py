#######################################################
# Day of the year: writing and using your own functions
#######################################################
# Scenario
#
# Your task is to write and test a function which takes three arguments (a year, a
# month, and a day of the month) and returns the corresponding day of the year, or
# returns None if any of the arguments is invalid.
# 
# Use the previously written and tested functions. Add some test cases to the code. This
# test is only a beginning.

def is_year_leap(year):
    # Note: the scenario where year is not a valid integer is not handled :(
    return year % 4 == 0


def days_in_month(year, month):
    # Note: the scenario where year is invalid or if month is not an integer between 1
    # and 12 inclusive is not handled :(
    months_with_twentysomething_days = [2]
    months_with_thirty_days = [4,6,9,11]
    months_with_thirtyone_days = [1,3,5,7,8,10,12]
    
    if month in months_with_twentysomething_days:
        if is_year_leap(year):
            return 29
        else:
            return 28
    
    if month in months_with_thirty_days:
        return 30
    
    if month in months_with_thirtyone_days:
        return 31


def day_of_year(year, month, day):
    # 1. if any input values are invalid, return None.
    # 1.1 if year is invalid, return None.
    # TODO: what is an invalid year?

    # 1.2 if month is invalid, return None.
    # TODO: what is an invalid month?

    # 1.3 if day is invalid, return None.
    # TODO: what is an invalid day (of month)?

    # 2. find out how many days have passed before this current day

    # 3. return the result


result = day_of_year(2021, 12, 31)
print("Result is: ", result)
